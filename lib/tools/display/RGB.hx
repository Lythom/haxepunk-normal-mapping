package tools.display;

class RGB {
	public function new(?rgb:Dynamic) {
		if (rgb != null) {
			this.r = rgb.r;
			this.g = rgb.g;
			this.b = rgb.b;
		} else {
			this.r = 0;
			this.g = 0;
			this.b = 0;
		}
		
	}
	
	public function equals(other:RGB):Bool {
		return r == other.r && b == other.b && g == other.g;
	}
	
    public var r:Float;
    public var g:Float;
    public var b:Float;
}