package com.haxepunk.normalmap;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import com.haxepunk.HXP;
import com.haxepunk.normalmap.Light;
import haxe.FastList;
import haxe.FastList;
import nme.display.BitmapData;
import nme.geom.ColorTransform;
import nme.geom.Point;
import nme.geom.Rectangle;
import nme.geom.Vector3D;
import nme.Vector;
import tools.display.ColorConverter;
import tools.display.RGB;
import tools.FastMath;

/**
 * ...
 * @author Samuel Bouchet
 */

class NormalMappedImage extends Image
{
	
	private var _mapSource:BitmapData;
	private var _lights:Array<Light>;
	private var _fastLights:FastList<Light>;
	private var _parentEntity:Entity;
	private var _lastEntityLightAngles:Array<Vector3D>;
	private var _lastLightProperties:Array<Light>;
	

	/**
	 * Constructor.
	 * @param	source		Source image.
	 * @param 	mapSource
	 * @param	lights
	 * @param	clipRect	Optional rectangle defining area of the source image to draw.
	 * @param	name		Optional name, necessary to identify the bitmapData if you are using flipped
	 */
	public function new(parentEntity:Entity, source:Dynamic, mapSource:Dynamic, lights:Array<Light>, clipRect:Rectangle = null, name:String = "")
	{
		if (Std.is(mapSource, BitmapData))
		{
			_mapSource = cast(mapSource, BitmapData);
		}
		else if (Std.is(mapSource, String)){
			_mapSource = HXP.getBitmap(mapSource);
		}
		if (_mapSource == null) throw "Invalid map source image.";
		_lights = lights;
		_fastLights = new FastList<Light>();
		_parentEntity = parentEntity;
		
		
		super(source, clipRect, name);
		
		sourcePixels = new FastList<UInt>();
		var v:Vector<UInt> = _source.getVector(_sourceRect);
		for (ipx in 0...v.length) {
			sourcePixels.add(v[v.length-ipx-1]);
		}
		normalPixels = _mapSource.getVector(_sourceRect);
		bufferPixels = _buffer.getVector(_sourceRect);
		resultPixels = new Vector<UInt>();
		
		mapAngle = new Vector3D();
		pixelAngle = new Vector3D();
		pixelColors = ColorConverter.toRGB(0);
		bufferPixelColors = ColorConverter.toRGB(0);
		newBufferPixelColors = ColorConverter.toRGB(0);
		
		_lastEntityLightAngles = new Array<Vector3D>();
		_lastLightProperties = new Array<Light>();
		
		updateBuffer();
	}
	
	override public function updateBuffer(clearBefore:Bool = false):Dynamic 
	{
		if (_source == null) return;
		applyNormalMapping(clearBefore);
		if (_tint != null) _buffer.colorTransform(_bufferRect, _tint);
	}
	
	private var mapAngle:Vector3D;
	private var pixelAngle:Vector3D;
	private var pixelColors:RGB;
	private var bufferPixelColors:RGB;
	private var newBufferPixelColors:RGB;
	private var angleDiff:Float;
	private var lightIntensity:Float;
	private var finalIntensity:Float;
	
	private var sourcePixels:FastList<UInt>;
	private var normalPixels:Vector<UInt>;
	private var bufferPixels:Vector<UInt>;
	private var resultPixels:Vector<UInt>;
	
	private function applyNormalMapping(force:Bool) 
	{
		if (sourcePixels == null) return;
		
		// for each light, update infos
		var changes:Bool = updateLightsInfo();
		
		if (changes || force) {
			// paint the black silhouette
			_buffer.copyPixels(_source, _sourceRect, HXP.zero);
			_buffer.colorTransform(_bufferRect, new ColorTransform(0, 0, 0, 1));
			doNormalMapping();
		}
	}
	
	private function doNormalMapping():Void 
	{
		
		var pixelColor:Int;
		var mapPixelColor:Int;
		var bufferPixelColor:Int;
		var pxIntensity:Float;
		
		var i:Int = 0;
		var iLight:Int = 0;
		var m = Math;
		var fm = FastMath;
		var pxX:Int;
		var pxY:Int;
		//for each pixel
		for (pixelColor in sourcePixels){
			// calculate the illumination of the pixel depending on normalMap
			mapPixelColor = normalPixels[i];
			bufferPixelColor = bufferPixels[i];
			
			var alpha:Float = ((pixelColor >> 24) & 255) / 255;
			// transparent pixels are ignored
			if (alpha > 0) {
				pxX = i % Math.floor(_sourceRect.width);
				pxY = Math.floor(i / _sourceRect.width);

				// multiply by 2 to approximate normalized vector.
				// amount of red gives X-axis angle
				mapAngle.x = (((mapPixelColor >> 16) & 255) / 255 - 0.5);
				// amount of green gives Y-axis angle
				mapAngle.y = (((mapPixelColor >> 8) & 255) / 255 - 0.5);
				// amount of blue gives Z-axis angle
				mapAngle.z = ((mapPixelColor & 255) / 255 - 0.5);
				mapAngle.normalize();
				
				ColorConverter.setRGB(pixelColor, pixelColors);
				ColorConverter.setRGB(bufferPixelColor, bufferPixelColors);
				ColorConverter.setRGB(bufferPixelColor, newBufferPixelColors);
				
				for (l in _fastLights) {
					pxIntensity = l.getIntensityAt(Math.floor(pxX + _parentEntity.x), Math.floor(pxY + _parentEntity.y));
					if (pxIntensity > 0) {
						// accurate position of light for each pixel
						pixelAngle.x = (_parentEntity.x + pxX - l.position.x);
						pixelAngle.y = (_parentEntity.y + pxY - l.position.y);
						pixelAngle.z = (_parentEntity.layer - l.position.z);
						pixelAngle.normalize();
						
						// lower value means higher exposure and higher values means lower exposure
						// value between 0 and Math.PI, reduced between 0 and 1.
						angleDiff = fm.acos(mapAngle.dotProduct(pixelAngle)) / (m.PI);
						//angleDiff = m.abs(Vector3D.angleBetween(mapAngle, pixelAngle) / m.PI);
						
						// apply angle exposure
						finalIntensity = pxIntensity * angleDiff;
						
						if (finalIntensity > 0) {
							// calculate new color
							newBufferPixelColors.r = Math.min(newBufferPixelColors.r + (pixelColors.r * l.colorAsRGB.r) * finalIntensity, 1);
							newBufferPixelColors.g = Math.min(newBufferPixelColors.g + (pixelColors.g * l.colorAsRGB.g) * finalIntensity, 1);
							newBufferPixelColors.b = Math.min(newBufferPixelColors.b + (pixelColors.b * l.colorAsRGB.b) * finalIntensity, 1);
						}					
					}
				}
				
				// set new color, keep alpha unchanged
				if (!newBufferPixelColors.equals(bufferPixelColors)) {
					_buffer.setPixel(i%m.floor(_sourceRect.width), m.floor(i/_sourceRect.width), ColorConverter.toInt(newBufferPixelColors));
				}
			}
			i++;
		}
	}
		
	
	
	private function updateLightsInfo():Bool 
	{
		var change:Bool = false;
		var i:Int = 1;
		var lastLightCount:Int = 0;
		
		// check for light change in the array
		while (_fastLights.first() != null) {
			// same instances ?
			if (_fastLights.pop() != lights[lights.length-i]) {
				change = true;
			}
			i++;
			lastLightCount++;
		}
		// same light number ?
		if (lastLightCount != lights.length) {
			change = true;
		}
		
		// record angles to avoid recalculate when the angle doesn't change
		// update the history list if needed
		while (_lastEntityLightAngles.length < lights.length) {
			_lastEntityLightAngles.push(new Vector3D());
		}
		while (_lastLightProperties.length < lights.length) {
			_lastLightProperties.push(new Light());
		}
		
		i = 0;
		for (l in _lights) {
			// calculate light values
			_fastLights.add(l);
			
			if (_lastEntityLightAngles[i].x != _parentEntity.x - l.position.x) {
				_lastEntityLightAngles[i].x = _parentEntity.x - l.position.x;
				change = true;
			}
			if (_lastEntityLightAngles[i].y != _parentEntity.y - l.position.y) {
				_lastEntityLightAngles[i].y = _parentEntity.y - l.position.y;
				change = true;
			}
			if (_lastEntityLightAngles[i].z != _parentEntity.layer - l.position.z) {
				_lastEntityLightAngles[i].z = _parentEntity.layer - l.position.z;
				change = true;
			}
			if (_lastLightProperties[i].intensity != l.intensity) {
				_lastLightProperties[i].intensity = l.intensity;
				change = true;
			}
			if (_lastLightProperties[i].radius != l.radius) {
				_lastLightProperties[i].radius = l.radius;
				change = true;
			}
			if (_lastLightProperties[i].color != l.color) {
				_lastLightProperties[i].color = l.color;
				change = true;
			}
			i++;
		}
		
		return change;
	}
	
	private function get_mapSource():Dynamic 
	{
		return _mapSource;
	}
	
	private function set_mapSource(value:Dynamic):Dynamic 
	{
		return _mapSource = value;
	}
	
	public var mapSource(get_mapSource, set_mapSource):Dynamic;
	
	private function get_lights():Array<Light> 
	{
		return _lights;
	}
	
	private function set_lights(value:Array<Light>):Array<Light> 
	{
		return _lights = value;
	}
	
	public var lights(get_lights, set_lights):Array<Light>;
	
	private function get_parentEntity():Entity 
	{
		return _parentEntity;
	}
	
	private function set_parentEntity(value:Entity):Entity 
	{
		return _parentEntity = value;
	}
	
	
	
	public var parentEntity(get_parentEntity, set_parentEntity):Entity;
	
}