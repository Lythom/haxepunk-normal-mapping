package world;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.HXP;
import com.haxepunk.normalmap.Light;
import com.haxepunk.normalmap.NormalMappedImage;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.World;
import nme.events.Event;

/**
 * ...
 * @author Samuel Bouchet
 */

class NormalMappingTestWorld extends World
{
	private var coneGraph:NormalMappedImage;
	private var lights:Array<Light>;
	private var coneGraph2:NormalMappedImage;
	private var cone:Entity;
	private var plantAnimation:NormalMappedImage;
	private var plant:Entity;
	private var testImage:NormalMappedImage;
	
	public static var instance:World ;

	public function new() 
	{
		super();
		//initHaxepunkGui();
		
		lights = new Array<Light>();
		lights.push(new Light(0, 0, 0xFFF8CA, 200, 1.5));
		lights.push(new Light(50, 50, 0xE10005, 200, 1, 0));
		//lights.push(new Light(200, 50, 0x2FDC10, 200, 1, 0));
		
		cone = new Entity();
		coneGraph = new NormalMappedImage(cone, HXP.getBitmap("gfx/cone.png"), HXP.getBitmap("gfx/cone_map.png"), lights);
		
		coneGraph2 = new NormalMappedImage(cone, HXP.getBitmap("gfx/cone.png"), HXP.getBitmap("gfx/cone_map2.png"), lights);
		
		cone.graphic = coneGraph;
		cone.x = 100;
		cone.y = 100;
		add(cone);
		
		lights[0].position.z = cone.layer + 5;

		plant = new Entity();
		plantAnimation = new NormalMappedImage(plant, HXP.getBitmap("gfx/planteBasique.png"), HXP.getBitmap("gfx/planteBasique_map.png"), lights);
		plant.graphic = plantAnimation;
		plant.x = 250;
		plant.y = 150;
		add(plant);
		
		var test = new Entity();
		testImage = new NormalMappedImage(test, HXP.getBitmap("gfx/test.png"), HXP.getBitmap("gfx/test_Normal.png"), lights);
		test.graphic = testImage;
		test.x = 200;
		test.y = 50;
		add(test);

		instance = this;
	}
	
	private function swfLoaded(e:Event):Void 
	{
		
	}

	override public function update() 
	{
		lights[0].position.x = Input.mouseX;
		lights[0].position.y = Input.mouseY;
		if (cone.graphic == coneGraph) {
			coneGraph.updateBuffer();
		} else if (cone.graphic == coneGraph2) {
			coneGraph2.updateBuffer();
		}
		if (Input.pressed(Key.NUMPAD_1)) {
			cone.graphic = coneGraph;
		} else if (Input.pressed(Key.NUMPAD_2)) {
			cone.graphic = coneGraph2;
		}
		
		plantAnimation.updateBuffer();
		testImage.updateBuffer();
		
		super.update();
	}
	
	override public function begin():Dynamic 
	{
		super.begin();
	}
	
	override public function end():Dynamic 
	{
		super.end();
	}
	
}