import com.haxepunk.Engine;
import com.haxepunk.HXP;
import com.haxepunk.utils.Key;
import com.haxepunk.World;
import world.WelcomeWorld;

class Main extends Engine
{

	public static inline var kScreenWidth:Int = 960;
	public static inline var kScreenHeight:Int = 640;
	public static inline var kFrameRate:Int = 60;
	public static inline var kClearColor:Int = 0x333333;
	public static inline var kProjectName:String = "HaxePunk";

	public function new()
	{
		super(kScreenWidth, kScreenHeight, kFrameRate, true);
	}

	override public function init()
	{
		HXP.console.enable();
		HXP.console.toggleKey = Key.P;

		HXP.screen.color = kClearColor;
		HXP.screen.scale = 2;
		HXP.world = new NormalMappingTestWorld();
	}

	public static function main()
	{
		new Main();
	}

}